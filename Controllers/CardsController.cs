using System;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ORM_library.Objects;
using ORM_library.Tables;
using WebApplication2.Models;
using WebApplication2.ViewModels;

namespace WebApplication2.Controllers{
    public class CardsController : Controller{


        //GET: Cards
        public IActionResult Index() {
            var bds = new DecksTable().Select_best();
            var ds = new DecksTable().Select();

            var cs = new CardsTable().Select_cards_most_rating_player();

            var model = new CardIndexForm {
                CardList = new CardListViewModel {Cards = GimmeCards(cs)},
                DeckList = new DeckListViewModel {Decks = DecksController.GimmeDecks(bds)},
                DeckSelect = new DeckSelectViewModel {Decks = new SelectList(DecksController.GimmeDecks(ds), "DeckID", "Name")}


            };

            
            return View("Index",model);
        }
        [HttpPost]
        public RedirectToActionResult Index(CardIndexForm model, string CardID) { //Jak sem dostat CardID z několika submitů - každý ho má jiné
            if ( CardID != "0") {
                Console.WriteLine("------------------------------FKLIDU-----------------------------------"+model.DeckSelect.SelectedDeck.DeckID);
            }
            else {
                Console.WriteLine("-------------------------------CARDID=0?-------------------------------------");
            }

            return RedirectToAction("Index");
        }
        
/*    public IActionResult AddCardToDeck(int DeckID, int CardID) {
        
        var model = new CardDeck {CardID = CardID, DeckID = DeckID, Count = 1};
          
            Console.WriteLine("------------------------D"+DeckID+", C"+CardID);
            return View("AddCardToDeck",model);
            
        }

    [HttpPost]
    public RedirectToActionResult AddCardToDeck(CardDeck model, int DeckID, int CardID) {
        var model = new CardDeck {CardID = CardID, DeckID = DeckID, Count = 1};
        return RedirectToAction("AddCardToDeck");
    }*/

        //GET: Cards/detail?multiverseid=3
        public IActionResult Detail(int multiverseID) {
            ViewData["MultiverseID"] = multiverseID;
            try {

                var card = new CardsTable().Select_by_id(multiverseID);

                var karta = new Card {
                    CardID = multiverseID,
                    Name = card.name,
                    Type = card.type,
                    Color = card.color,
                    Artist = card.artist,
                    Image = card.image,
                    CMC = card.cmc,
                    Rarity = card.rarity,
                    Power = card.power,
                    Toughness = card.toughness,
                    SetCode = card.set_code,
                    DeletedAt = card.deleted_at
                };


                return View(karta);
            }
            catch {
                return View(new Card {
                    Artist = "Not in database", CardID = multiverseID, CMC = 666, Color = "Not in database",
                    DeletedAt = null, Image = "Not in database", Name = "Not in database", SetCode = "Not in database",
                    Rarity = 0, Type = "Not in database"
                });
            }
            
        }

//GET: Cards/cardslist
        public IActionResult CardsList() {
            var cs = new CardsTable().Select();

            CardListViewModel model = new CardListViewModel {Cards = GimmeCards(cs)};
            return View(model);
        }

//GET:Cards/CardSearch
        public IActionResult CardsSearch(string searchStr) {
            ViewData["SearchStr"] = searchStr;
            var cs = new CardsTable().Select_by_name(searchStr);

            CardListViewModel model = new CardListViewModel {Cards = GimmeCards(cs)};
            return View(model);
        }

      

        public static Collection<Card> GimmeCards(Collection<Cards> cardsTableSelect) {
            var cards = new Collection<Card>();
            foreach (var c in cardsTableSelect) {
                var card = new Card {
                    CardID = c.card_id,
                    Name = c.name,
                    Type = c.type,
                    Color = c.color,
                    Artist = c.artist,
                    Image = c.image,
                    CMC = c.cmc,
                    Rarity = c.rarity,
                    Power = c.power,
                    Toughness = c.toughness,
                    SetCode = c.set_code,
                    DeletedAt = c.deleted_at
                };
                cards.Add(card);
            }

            return cards;
        }

    }
}