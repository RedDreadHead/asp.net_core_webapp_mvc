using System.Collections.Generic;
using System.Collections.ObjectModel;
using ORM_library.Objects;
using WebApplication2.Models;

namespace WebApplication2.Controllers{
    public class CommentsController{
        public static IEnumerable<Comment> GimmeComments(IEnumerable<Comments> commentsTableSelect) {
            var comments = new Collection<Comment>();
            foreach (var c in commentsTableSelect) {
                var com = new Comment {
                    AddedAt=c.added_at,
                    CommentID = c.comment_id,
                    DeckID=c.deck_id,
                    DeletedAt = c.deleted_at,
                    PlayerID = c.player_id,
                    Text = c.text
                }; 
                comments.Add(com);
            }

            return comments;
        }
    }
}