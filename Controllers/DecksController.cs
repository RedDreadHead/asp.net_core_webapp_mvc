using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ORM_library.Objects;
using ORM_library.Tables;
using WebApplication2.Models;
using WebApplication2.ViewModels;


namespace WebApplication2.Controllers{
    public class DecksController : Controller{
        // GET
        public IActionResult Index() {
            var ds = new DecksTable().SelectWithoutDeleted();
            var model = new DeckListViewModel {Decks = GimmeDecks(ds)};
            return View(model);
        }

        // GET Decks/Detail
        public IActionResult Detail(int id) {
            var model = new DeckDetailForm();
            fillModel(model, id);

            return View("Detail", model);
        }

        [HttpPost]
        public RedirectToActionResult Detail(DeckDetailForm model, int id) {
            if (model.Rating != null) {
                var rat = new Ratings(model.Rating.Stars, model.Rating.PlayerID, id);
                Console.WriteLine(rat);
                 new RatingsTable().Rate(rat);

            }
            else{
           
            Change change = null;
           /* if (string.IsNullOrEmpty(model.Comment.Text)) {
                ModelState.AddModelError("Comment.Text","You have to type some text to add a comment");
            }

            if (model.Comment.PlayerID == -1) {
                ModelState.AddModelError("Comment.PlayerID","You have to select the author");

            }*/
            Console.WriteLine("---------------------------------------------------------------------------------");
            ModelState.Remove("Change.Count");
            ModelState.Remove("Change.Card.CardID");
            if (ModelState.IsValid) {
                var authorID = model.Comment.PlayerID;
                var text = model.Comment.Text;
                bool recChange = model.Comment.isChangeRecommended;
                var c = new Comments {
                    added_at = DateTime.Now, deck_id = id, player_id = authorID,
                    text = model.Comment.Text, deleted_at = null
                };
                
//comment insert
                new CommentsTable().Add_comment(c);
                
                var comid = new CommentsTable().Select_max_ID();
                
                if (recChange) {
                   // if (ModelState["Change.Count"].ValidationState == ModelValidationState.Valid) {
                      //  if (ModelState["Change.Card.CardID"].ValidationState == ModelValidationState.Valid) {
                            change = new Change {
                                Card = model.Change.Card,
                                CardID = model.Change.Card.CardID,
                                ChangeType = model.Change.ChangeType,
                                Comment = model.Comment,
                                CommentID = comid,
                                Count = model.Change.Count
                            };
//Change insert
                            new ChangesTable().Insert(new Changes(change.ChangeType,change.CardID, comid, change.Count ));
                        //}
                       // else {ModelState.AddModelError("Change.Count","Počet karet musí být mezi 0 a 4 pokud karta není land");}
                        
                   // }
                    //else {ModelState.AddModelError("Change.Card.CardID","You must select card for a change");}
                    

                    Console.WriteLine("Text=" + text + ",author=" + authorID + ", Change recommended\n" +
                                      "\tChange type=" + change.ChangeType + ", for card:" + change.CardID + ": " +
                                      change.Count + "times");
                }
                else {
                    Console.WriteLine("Text=" + text + ",author=" + authorID + ", Change not recommended");
                }


                

                /*    if (model.Comment.isChangeRecommended) {
                        Console.WriteLine(
                            new ChangesTable().Insert(
                                new Changes(
                                    model.Change.ChangeType,
                                    model.CardSelect.SelectedCard.CardID,
                            comid,
                                    model.Change.Count
                        )));
                        
                    }*/
/*                Console.WriteLine(now+": CommentID:"+comid+
                                  ", DeckID:"+did+", PlayerID:"+authorID+", text:"+text);
*/
                return RedirectToAction("Detail");
            }}

            

            Console.WriteLine("---------------------------------------------------------------------------------");


            return RedirectToAction("Detail", model);
            //return View("Detail",model);
        }

        public IActionResult Create() {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Deck model) {
            try {
                if (!ModelState.IsValid) return View();
                ViewData["Name"] = model.Name;
                ViewData["Description"] = model.Description;
                ViewData["PlayerID"] = model.PlayerID;

                return View("Index");
            }
            catch {
                return View();
            }
        }

        private DeckDetailForm fillModel(DeckDetailForm model, int ID) {
            var ct = new CardsTable();
            var pt = new PlayersTable();
            var cdt = new Card_deckTable();
            Dictionary<Card, int> cardsWCount = new Dictionary<Card, int>();

            var cds = cdt.Select();
            var ps = pt.Select(); //Select všech hráčů
            var d = new DecksTable().Select(ID); //Select decku
            var cs = new CardsTable().Select_by_deck_id(ID); //Select karet v decku
            var coms = new CommentsTable().Select_by_deck_id(ID); //Select komentářů k decku
        
            var avgCmc = ct.Select_avg_cmc(ID);
            var typeRat = ct.Select_type_ratio(d);
            var coloreRat = ct.Select_color_ratio(d);
            
            
            var cards = CardsController.GimmeCards(cs);
            var players = PlayersController.GimmePlayers(ps);
            var comments = CommentsController.GimmeComments(coms);
             foreach (var c in comments) {
                 var author = pt.Select_by_id(c.PlayerID);
                 c.Author = new Player {
                     DeletedAt = author.deleted_at, Login = author.login, Note = author.note,
                     Password = author.password, PlayerID = author.player_id, Registered = author.registered
                 };
             }
            
             var ranks = new RanksTable().Select();
             var rank = "No rank yet";
             if (d.stars.HasValue) {
                rank = ranks[(int) d.stars].rank_name;}
             
            var stats = new Statistics { AvgCmc = avgCmc, ColorRatio = coloreRat, TypeRatio = typeRat };
            var deck = new Deck {
                CardsCount = d.cards_count,
                DeckID = d.deck_id,
                Description = d.description,
                Name = d.name,
                MaxCardsCount = d.max_cards_count,
                Stars = d.stars,
                PlayerID = d.player_id,
                DeletedAt = d.deleted_at,
                Statistics = stats,
                Rank = rank
            };

            foreach (var c in cards) {
                foreach (var l in cds) {
                    if (l.deck_id == ID && l.card_id == c.CardID) {
                        cardsWCount.Add(c,l.count);
                    }
                }
            }
            var karty = new CardListViewModel {Cards = cards, CardsCount = cardsWCount}; //List ze selectu karet
            var balicek = deck; //Model deck z DAO decku
            var hraciselect = new PlayerSelectViewModel //Dropdown List ze selectu hráčů
                {Players = new SelectList(players, "PlayerID", "Login")};
            var komenty = new CommentListViewModel {Comments = comments}; //List ze selectu komentářů
            var kartyselect = new CardSelectViewModel //Dropdown list ze selectu karet
                {Cards = new SelectList(cards, "CardID", "Name")};

           
            model.PlayerSelect = hraciselect;
            model.Deck = balicek;
            model.CardList = karty;
            model.CardSelect = kartyselect;
            model.CommentList = komenty;

            return model;
        }

        public static IEnumerable<Deck> GimmeDecks(IEnumerable<Decks> decksTableSelect) {
            var decks = new Collection<Deck>();
            foreach (var d in decksTableSelect) {
                var deck = new Deck {
                    CardsCount = d.cards_count,
                    DeckID = d.deck_id,
                    Description = d.description,
                    Name = d.name,
                    MaxCardsCount = d.max_cards_count,
                    Stars = d.stars,
                    PlayerID = d.player_id,
                    DeletedAt = d.deleted_at
                };
                decks.Add(deck);
            }

            return decks;
        }
    }
}