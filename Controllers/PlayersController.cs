using System.Collections.Generic;
using System.Collections.ObjectModel;
using ORM_library.Objects;
using WebApplication2.Models;

namespace WebApplication2.Controllers{
    public class PlayersController{
        public static IEnumerable<Player> GimmePlayers(IEnumerable<Players> playersTableSelect) {
            var players = new Collection<Player>();
            foreach (var p in playersTableSelect) {
                var pp = new Player {
                    DeletedAt = p.deleted_at,
                    Login = p.login,
                    Note = p.note,
                    Password = p.password,
                    PlayerID = p.player_id,
                    Registered = p.registered
                };
                players.Add(pp);
            }

            return players;
        }
    }
}   
