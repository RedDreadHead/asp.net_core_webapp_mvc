using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication2.Models{
    public class Card{
        public int CardID { get; set; }
        [Display(Name = "Card Name")]
        public string Name { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public string Artist { get; set; }
        public string Image { get; set; }
        public int CMC { get; set; }
        public int Rarity { get; set; } 
        public int? Power { get; set; }
        public int? Toughness { get; set; }
        public string SetCode { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DeletedAt { get; set; }

        //public Set set { get; set; }



        public override string ToString() {
            return Name;}
        
    }
}