namespace WebApplication2.Models{
    public class Change{
        public string ChangeType { get; set; }
        public int CardID{ get; set; }
        public int CommentID { get; set; }
        public int Count { get; set; }
        
        public Card Card { get; set; }
        public Comment Comment { get; set; }
    }
}