using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models{
    public class Comment{
        public int CommentID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage="You can't add an empty comment")]
        public string Text { get; set; }
        public DateTime AddedAt { get; set; }
     
        public int PlayerID { get; set; }
        public Player Author { get; set; }
        public int DeckID { get; set; }  
        [DataType(DataType.Date)]
        public DateTime? DeletedAt { get; set; }
        
        public bool isChangeRecommended { get; set; }
        
    }
}