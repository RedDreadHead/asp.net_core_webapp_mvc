using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models{
    public class Deck{
        public int DeckID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int MaxCardsCount { get; set; }
        public int CardsCount { get; set; }
        public int? Stars { get; set; }
        public int PlayerID { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DeletedAt{ get;  set; }
        
        public Collection<Comment> Comments{get;set;}
        
        public string Rank { get; set; }
        
        public Statistics Statistics { get; set; }
    }
}