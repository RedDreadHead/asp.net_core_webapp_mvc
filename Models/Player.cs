using System;

namespace WebApplication2.Models{
    public class Player{
        public int PlayerID {get; set;}
        public string Login { get; set; }
        public DateTime Registered { get; set; }
        public string Password { get; set; }
        public DateTime? DeletedAt { get; set; }
        public string Note { get; set; }
        
    }
}