namespace WebApplication2.Models{
    public class Rating{
        public int Stars { get; set; }
        public int PlayerID { get; set; }
        public int DeckID { get; set; }

    }
}