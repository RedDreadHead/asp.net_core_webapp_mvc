using System.Collections.Generic;

namespace WebApplication2.Models{
    public class Statistics{
        public string AvgCmc;
        public List<Dictionary<string, string>> TypeRatio;
        public List<Dictionary<string, string>> ColorRatio;
        
    }
}