namespace WebApplication2.ViewModels{
    public class CardIndexForm{
        public CardListViewModel CardList { get; set; }
        public DeckListViewModel DeckList { get; set; }
        public DeckSelectViewModel DeckSelect { get; set; }
   }
}