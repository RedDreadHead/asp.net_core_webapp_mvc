using WebApplication2.Models;

namespace WebApplication2.ViewModels{
    public class DeckDetailForm{
        public CardListViewModel CardList { get; set; }
        public CardSelectViewModel CardSelect { get; set; }
        public CommentListViewModel CommentList{ get; set; }
        public Deck Deck{ get; set; }
        public PlayerSelectViewModel PlayerSelect{ get; set; }
        
        public Change Change{ get; set; }
        public Comment Comment{ get; set; }
        
        public Rating Rating { get; set; }
        
    }
}