using System.Collections.Generic;
using WebApplication2.Models;

namespace WebApplication2.ViewModels{
    public class CardListViewModel{
        public IEnumerable<Card> Cards { get; set; }
        public Dictionary<Card, int> CardsCount { get; set; }
    }
}