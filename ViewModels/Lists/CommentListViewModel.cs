using System.Collections.Generic;
using WebApplication2.Models;

namespace WebApplication2.ViewModels{
    public class CommentListViewModel{
        public IEnumerable<Comment> Comments;
    }
}