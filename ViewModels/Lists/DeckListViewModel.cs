using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication2.Models;

namespace WebApplication2.ViewModels{
    public class DeckListViewModel{
        public IEnumerable<Deck> Decks { get; set; }
    }
}