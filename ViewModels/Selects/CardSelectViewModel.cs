using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication2.Models;

namespace WebApplication2.ViewModels{
    public class CardSelectViewModel{
        public IEnumerable<SelectListItem> Cards { get; set; }
        public Card SelectedCard { get; set; }
    }
}