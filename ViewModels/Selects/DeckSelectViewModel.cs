using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication2.Models;

namespace WebApplication2.ViewModels{
    public class DeckSelectViewModel{
        public IEnumerable<SelectListItem> Decks { get; set; }
        public Deck SelectedDeck { get; set; }
    }
}