using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication2.Models;

namespace WebApplication2.ViewModels{
    public class PlayerSelectViewModel{
        public IEnumerable<SelectListItem> Players { get; set; }
        public Player SelectedPlayer { get; set; }
    }
}