#pragma checksum "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "36f501753bf09c5e037e96617e2ade29ee9bd903"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Cards_Detail), @"mvc.1.0.view", @"/Views/Cards/Detail.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Cards/Detail.cshtml", typeof(AspNetCore.Views_Cards_Detail))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/_ViewImports.cshtml"
using WebApplication2;

#line default
#line hidden
#line 2 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/_ViewImports.cshtml"
using WebApplication2.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"36f501753bf09c5e037e96617e2ade29ee9bd903", @"/Views/Cards/Detail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"46b9317e42a2d2da7e4eccd375bc9e60c2bd0cdc", @"/Views/_ViewImports.cshtml")]
    public class Views_Cards_Detail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Card>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
  

    var id = @ViewData["MultiverseID"];
    ViewData["Title"] = Model.Name;
    ViewData["Image"] = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + id + "&type=card";
    

#line default
#line hidden
            BeginContext(216, 164, true);
            WriteLiteral("\r\n\r\n<table >\r\n    \r\n    <tr>\r\n        <td >\r\n\r\n            <!--Vlevo nahoře-->\r\n            Detail of card\r\n            <h1 style=\"color:#acdc42\" class=\"display-4\">");
            EndContext();
            BeginContext(381, 17, false);
#line 18 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
                                                   Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(398, 22, true);
            WriteLiteral("</h1>\r\n        </td>\r\n");
            EndContext();
            BeginContext(848, 185, true);
            WriteLiteral("        <td rowspan=\"2\">\r\n                    <!--Vpravo dole-->\r\n        <div class=\"text-center\">\r\n                    Card from URL (from link and id):<br/>\r\n                    <img");
            EndContext();
            BeginWriteAttribute("src", " src=", 1033, "", 1056, 1);
#line 34 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
WriteAttributeValue("", 1038, ViewData["Image"], 1038, 18, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1056, 304, true);
            WriteLiteral(@" alt=""CardImage""/><br/><br/>
        </div>
                </td>
    </tr>
    <tr>
        <td>
            <!--Vlevo dole-->

            Card Info from database:
            <table class=""table-striped"">
                <tr>
                    <td>Name:</td> <td style=""color:#eeeeee""><b>");
            EndContext();
            BeginContext(1361, 10, false);
#line 45 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
                                                           Write(Model.Name);

#line default
#line hidden
            EndContext();
            BeginContext(1371, 120, true);
            WriteLiteral("</b></td>\r\n                </tr>\r\n                <tr>\r\n                    <td >Set:</td> <td style=\"color:#eeeeee\"><b>");
            EndContext();
            BeginContext(1492, 13, false);
#line 48 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
                                                           Write(Model.SetCode);

#line default
#line hidden
            EndContext();
            BeginContext(1505, 121, true);
            WriteLiteral("</b></td>\r\n                </tr>\r\n                <tr>\r\n                    <td >Type:</td> <td style=\"color:#eeeeee\"><b>");
            EndContext();
            BeginContext(1627, 10, false);
#line 51 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
                                                            Write(Model.Type);

#line default
#line hidden
            EndContext();
            BeginContext(1637, 122, true);
            WriteLiteral("</b></td>\r\n                </tr>\r\n                <tr>\r\n                    <td >Color:</td> <td style=\"color:#eeeeee\"><b>");
            EndContext();
            BeginContext(1760, 11, false);
#line 54 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
                                                             Write(Model.Color);

#line default
#line hidden
            EndContext();
            BeginContext(1771, 123, true);
            WriteLiteral("</b></td>\r\n                </tr>\r\n                <tr>\r\n                    <td >Artist:</td> <td style=\"color:#eeeeee\"><b>");
            EndContext();
            BeginContext(1895, 12, false);
#line 57 "/home/rddrdhd/Documents/WebApplication2/WebApplication2/Views/Cards/Detail.cshtml"
                                                              Write(Model.Artist);

#line default
#line hidden
            EndContext();
            BeginContext(1907, 102, true);
            WriteLiteral("</b></td>\r\n                </tr>\r\n            </table>\r\n\r\n        </td>\r\n        \r\n    </tr>\r\n</table>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Card> Html { get; private set; }
    }
}
#pragma warning restore 1591
